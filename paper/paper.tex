\documentclass[runningheads,a4paper]{llncs}

\usepackage{amsmath}
\usepackage{amssymb}
\setcounter{tocdepth}{3}
\usepackage{graphicx}
\usepackage{minted}

% For algorithms
\usepackage{algorithm}
\usepackage{algorithmic}

\usepackage{hyperref}
\newcommand{\theHalgorithm}{\arabic{algorithm}}

\usepackage{url}

% Additional packages 
\usepackage{amsfonts}
%\usepackage{amsthm}
\usepackage{float}
\usepackage{enumitem}

\floatstyle{ruled}
\newfloat{Program}{ht}{lop}

\newtheorem{dfn}{Definition}
\newtheorem{thm}{Theorem}
\newtheorem{lmm}{Lemma}
\newtheorem{crl}{Corollary}

% Indicator function
% I failed to find a font package that is available 
% both in Ubuntu and in Mac OS X and has fancy 1.
\newcommand {\I} {\ensuremath {\mathbf{1\hspace{-5.5pt}1}}}


\urldef{\mailsa}\path|{dtolpin,jwvdm,fwood}@robots.ox.ac.uk|
\newcommand{\keywords}[1]{\par\addvspace\baselineskip
\noindent\keywordname\enspace\ignorespaces#1}

\renewcommand{\vec}[1]{\ensuremath{\boldsymbol{#1}}}
\newcommand{\x}{\ensuremath{\vec{x}}}
\newcommand{\y}{\ensuremath{\vec{y}}}


\begin{document}

\mainmatter  % start of an individual contribution

% first the title is needed
\title{Probabilistic Programming in Anglican}

% a short form should be given in case it is too long for the running head
%\titlerunning{Anglican}

% the name(s) of the author(s) follow(s) next
%
% NB: Chinese authors should write their first names(s) in front of
% their surnames. This ensures that the names appear correctly in
% the running heads and the author index.
%
\author{David Tolpin \and Jan-Willem van de Meent \and Frank Wood}

%\authorrunning{ }

% the affiliations are given next; don't give your e-mail address
% unless you accept that it will be published
\institute{University of Oxford \\ Department of Engineering Science \\
\mailsa}
%\url{http://www.springer.com/lncs}}

%
% NB: a more complex sample for affiliations and the mapping to the
% corresponding authors can be found in the file "llncs.dem"
% (search for the string "\mainmatter" where a contribution starts).
% "llncs.dem" accompanies the document class "llncs.cls".
%

\toctitle{The Probabilistic Programming System Anglican}
\tocauthor{David Tolpin and Frank Wood}
\maketitle


\begin{abstract}
    Anglican is a probabilistic programming system designed to interoperate
    with Clojure and other JVM languages. We describe the implementation of
    Anglican and illustrate how its design facilitates both explorative and
    industrial use of probabilistic programming.
\end{abstract}

\keywords{Probabilistic programming}

\section{Introduction}

For data science practitioners, statistical inference is typically
but one step in a more elaborate analysis workflow. The first stage
of this work involves data acquisition, pre-processing and cleaning.
This is often followed by several iterations of exploratory model
design and testing of inference algorithms. Once a sufficiently
robust statistical model and corresponding inference algorithm have
been identified, analysis results must be post-processed, visualized,
and in some cases integrated into a wider production system.

Probabilistic programming systems \cite{GMR+08,MSP14,WVM14,GS15}
represent generative models as programs written in a specialized
language that provides syntax for the definition and conditioning of
random variables. The code for such models is generally concise,
modular, and easy to modify or extend. Typically inference can be
performed for any probabilistic program using one or more generic
inference techniques provided by the system backend, such as
Metropolis-Hastings \cite{WSG11,MSP14,YHG14}, Hamiltonian Monte Carlo
\cite{SDT04}, expectation propagation \cite{MWG+10}, and extensions
of Sequential Monte Carlo \cite{WVM14,MYM+15,PWD+14} methods.
Although these generic techniques are not always as statistically
efficient as techniques that take advantage of model-specific
optimizations, probabilistic programming makes it easier to optimize
models for a specific application in a manner that is efficient in
terms of the dimensionality of its latent variables.

While probabilistic programming systems shorten the iteration cycle in
exploratory model design, they typically lack basic functionality needed for
data I/O, pre-processing, and analysis and visualization of inference results.
In this demonstration, we describe the implementation of
Anglican~(\url{http://www.robots.ox.ac.uk/~fwood/anglican/}), a probabilistic
programming language that tightly integrates with
Clojure~(\url{http://clojure.org/}), a general-purpose programming language
that runs on the Java Virtual Machine (JVM). Both languages share a common
syntax, and can be invoked from each other. This allows Anglican programs to
make use of a rich set of libraries written in both Clojure and Java.
Conversely Anglican allows intuitive and compact specification of models for
which inference may be performed as part of a larger Clojure project.

\section{Design Outline}

An Anglican program, or \textit{query}, is compiled into a Clojure
function. When inference is performed with a provided algorithm, this
produces a sequence of return values, or \textit{predicts}. Anglican
shares a common syntax with Clojure; Clojure functions can be called
from Anglican code and vice versa. A simple program in Anglican can
look like the following code:

\begin{minted}[linenos]{clojure}
(defquery models
  "chooses a distribution which describes the data"
  (let [;; Model -- randomly choose a distribution and parameters
        dist (sample (categorical [[uniform-discrete 1]
                                   [uniform-continuous 1]
                                   [normal 1] 
                                   [gamma 1]]))
        a (sample (gamma 1 1)) b (sample (gamma 1 1))
        d (dist a b)]
    ;; Data  --- samples from the unknown distribution
    (observe d 1) (observe d 2) (observe d 4) (observe d 7)
    ;; Output --- predicted distribution type and parameters
    (predict :d (type d))
    (predict :a a) (predict :b b)))
\end{minted}

Internally, an Anglican query is represented by a computation in
\textit{continuation passing style} (CPS), and inference algorithms
exploit the CPS structure of the code to intercept probabilistic
operations in an algorithm-specific way. Among the available
inference algorithms there are Particle Cascade~\cite{PWD+14},
Lightweight Metropolis-Hastings~\cite{WSG11}, Iterative Conditional
Sequential Monte-Carlo (Particle Gibbs)~\cite{WVM14}, and others.
Inference on Anglican queries generates a lazy sequence of
samples, which can be processed asynchronously in Clojure
code for analysis, integration, and decision making.

Clojure (and Anglican) run on the JVM and get access to a wide choice of Java
libraries for data processing, networking, presentation, and imaging.
Conversely, Anglican queries can be called from Java and other JVM languages.
Programs involving Anglican queries can be deployed as JVM
\textit{jars}, and run without modification on any platform for 
which JVM is available.

\section{Usage Patterns}

Anglican is suited to rapid prototyping and exploration, on one hand,
and inclusion as a library into larger systems for supporting
inference-based decision making, on the other hand.

For exploration and research, Anglican can be run in Gorilla
REPL~(\url{http://gorilla-repl.org/}); a modified version of
Gorilla REPL better suited for Anglican is provided.  Gorilla
REPL is a notebook-style environment which runs in browser and
serves well as a workbench for rapid prototyping and
checking of ideas. Figure~\ref{fig:gorilla} shows a fragment of
an Anglican worksheet in the browser:

\begin{figure}[H]
	\centering
    \includegraphics[scale=0.75]{gorilla.pdf}
	\caption{Anglican worksheet fragment. Post-processed
		inference results shown in a plot.}
	\label{fig:gorilla}
\end{figure}

Library use is inherent to the Anglican's design for
interoperability with Clojure. An Anglican query, along with
supporting functions written in either Anglican or Clojure,
can be encapsulated in a Clojure module  and called from other
modules just like Clojure function. Additionally, Anglican
functions common for queries of a particular type or structure,
such as state-space models or decision-making queries, can be
wrapped as libraries and re-used.

\section{Anglican Examples}

Anglican benefits from a community-maintained collection of problem
examples~(\url{https://bitbucket.org/fwood/anglican-examples}),
styled as Gorilla REPL worksheets. Each example is a case
study of a problem involving probabilistic inference, includes
problem statement, explanations for the solution, and a graphical
presentation of inference results. Some of the
included examples are:
\begin{itemize}
    \item Indian GPA,
    \item Complexity Reduction,
    \item Bayes Net,
    \item Kalman Smoother,
    \item Gaussian Mixture Model,
    \item DP Mixture Model,
    \item Hierarchical Dirichlet Process,
    \item Probabilistic Deterministic Infinite Automata,
    \item Nested Number Guessing,
    \item Maximum Likelihood for Logistic Regression.
\end{itemize}
Anglican users are encouraged to contribute examples, both
demonstrating advantages of probabilistic programming and
presenting challenges to the current state-of-art of inference
algorithms, to the repository.

\section{Acknowledgments}
 
This work is supported under DARPA PPAML through the U.S. AFRL under
Cooperative Agreement number FA8750-14-2-0004.
% The U.S. Government is
% authorized to reproduce and distribute reprints for Governmental purposes
% notwithstanding any copyright notation heron. The views and conclusions
% contained herein are those of the authors and should be not interpreted as
% necessarily representing the official policies or endorsements, either
% expressed or implied, of DARPA, the U.S. Air Force Research Laboratory or the
% U.S. Government.

\bibliographystyle{splncs03}
\bibliography{refs}

%\clearpage
%\input{appendix}

\end{document}
